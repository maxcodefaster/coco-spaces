import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { LocationPage } from './location.page';

import { LocationPageRoutingModule } from './location-routing.module';
import {ComponentsModule} from "../components/components.module";

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    LocationPageRoutingModule,
    ComponentsModule

  ],
  declarations: [LocationPage]
})
export class Tab2PageModule {}

import {Component} from '@angular/core';
import {ModalController, ToastController} from "@ionic/angular";
import {Components} from "@ionic/core";
import {ChatModalPage} from "../chat-modal/chat-modal-page.component";
import {CanvasMatrixModalPage} from "../canvas-modal/canvas-matrix-modal-page.component";

@Component({
  selector: 'app-location',
  templateUrl: 'location.page.html',
  styleUrls: ['location.page.scss']
})
export class LocationPage {
  newLocation: boolean = true;
  roomList: boolean = false

  public constructor(
    private toastCtrl: ToastController,
    private modalCtrl: ModalController
  ) {
  }

  toggleLocation() {
    this.newLocation = !this.newLocation
    this.newLocation ? this.presentToast('Wilkommen in der Schanzenstraße 22') : this.presentToast('Wilkommen in der Schanzenstraße 28')
  }

  toggleRoomList() {
    this.roomList = !this.roomList
    if (this.roomList) {
      this.presentToast('Buche deinen Raum')
    }
  }

  async presentToast(msg: string) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  /* canvas matrix modal*/
  async openCanvas() {
    const canvasMatrixModal: Components.IonModal = await this.modalCtrl.create({
      component: CanvasMatrixModalPage,
      showBackdrop: false,
      componentProps: {}
    });
    await canvasMatrixModal.present();
  }


  /* chat modal*/
  async openChat() {
    const chat: Components.IonModal = await this.modalCtrl.create({
      component: ChatModalPage,
      showBackdrop: false,
      componentProps: {}
    });
    await chat.present();
  }

}

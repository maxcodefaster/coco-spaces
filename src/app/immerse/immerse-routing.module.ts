import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ImmersePage } from './immerse.page';

const routes: Routes = [
  {
    path: '',
    component: ImmersePage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ImmersePageRoutingModule {}

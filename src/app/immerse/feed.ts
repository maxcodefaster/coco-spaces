export const Feed = [
  {
    "title": "Maker Space",
    "src": "/assets/videos/crafting.mp4",
    "viewers": 0
  }, {
    "title": "Hallway",
    "src": "/assets/videos/hallway.mp4",
    "viewers": 0
  }, {
    "title": "Product Dimensions",
    "src": "/assets/videos/meeting.mp4",
    "viewers": 0
  }, {
    "title": "Big room",
    "src": "/assets/videos/far-away-meeting.mp4",
    "viewers": 0
  }, {
    "title": "C&R Jam Session",
    "src": "/assets/videos/jam.mp4",
    "viewers": 0
  }
]

import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ImmersePage } from './immerse.page';

import { ImmersePageRoutingModule } from './immerse-routing.module';
import {ComponentsModule} from "../components/components.module";

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild([{ path: '', component: ImmersePage }]),
    ImmersePageRoutingModule,
    ComponentsModule
  ],
  declarations: [ImmersePage]
})
export class Tab3PageModule {}

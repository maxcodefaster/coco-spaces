import {Component, OnInit, QueryList, Renderer2, ViewChild, ElementRef, ViewChildren} from '@angular/core';
import {ModalController} from "@ionic/angular";
import {Components} from '@ionic/core';
import {CanvasMatrixModalPage} from "../canvas-modal/canvas-matrix-modal-page.component";
import {ChatModalPage} from "../chat-modal/chat-modal-page.component";
import {Feed} from './feed'

@Component({
  selector: 'app-immerse',
  templateUrl: 'immerse.page.html',
  styleUrls: ['immerse.page.scss']
})
export class ImmersePage implements OnInit {
  @ViewChildren('player') videoPlayers: QueryList<any>
  feed = Feed;
  currentPlaying = null;

  @ViewChild('stickyplayer', {static: false}) stickyPlayer: ElementRef;
  stickyVideo: HTMLVideoElement = null;
  stickyPlaying: boolean = false;

  peopleWatching: number = 42;

  constructor(
    private renderer: Renderer2,
    private modalCtrl: ModalController
  ) {
  }

  ngOnInit() {
    this.updateFakeStats()
  }

  // video controls & scroll logic
  didScroll() {
    if (this.currentPlaying && this.isElementInViewport(this.currentPlaying)) {
      return;
    } else if (this.currentPlaying && !this.isElementInViewport(this.currentPlaying)) {
      // Item is out of view, pause it
      this.currentPlaying.pause();
      this.currentPlaying = null
    }
    this.videoPlayers.forEach(player => {
      if (this.currentPlaying) {
        return;
      }
      const nativeElement = player.nativeElement;
      const inView = this.isElementInViewport(nativeElement)
      if (this.stickyVideo && this.stickyVideo.src == nativeElement.src) {
        return;
      }
      if (inView) {
        this.currentPlaying = nativeElement;
        this.currentPlaying.muted = true;
        this.currentPlaying.play()
      }
    })
  }

  openFullscreen(elem) {
    if (elem.requestFullscreen) {
      elem.requestFullscreen()
    } else if (elem.webkitEnterFullscreen) {
      elem.webkitEnterFullscreen();
      elem.enterFullscreen()
    }
  }

  isElementInViewport(el) {
    const rect = el.getBoundingClientRect()
    return (
      rect.top >= 0 &&
      rect.left >= 0 &&
      rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
      rect.right <= (window.innerWidth || document.documentElement.clientWidth)
    )
  }


  // sticky player
  playOnSide(elem) {
    if (this.stickyVideo) {
      this.renderer.removeChild(this.stickyPlayer.nativeElement, this.stickyVideo);
    }
    this.stickyVideo = elem.cloneNode(true);
    this.renderer.appendChild(this.stickyPlayer.nativeElement, this.stickyVideo);
    if (this.currentPlaying) {
      const playPosition = this.currentPlaying.currentTime;
      this.currentPlaying.pause();
      this.currentPlaying = null;
      this.stickyVideo.currentTime = playPosition
    }
    this.stickyVideo.muted = false;
    this.stickyVideo.play()
    this.stickyPlaying = true;
  }

  closeSticky() {
    if (this.stickyVideo) {
      this.renderer.removeChild(this.stickyPlayer.nativeElement, this.stickyVideo)
      this.stickyVideo = null;
      this.stickyPlaying = false;
    }
  }

  playOrPauseSticky() {
    if (this.stickyPlaying) {
      this.stickyVideo.pause()
      this.stickyPlaying = false;
    } else {
      this.stickyVideo.play()
      this.stickyPlaying = true;
    }
  }


  // misc
  async updateFakeStats() {
    while (true) {
      this.peopleWatching = 0;
      this.feed.forEach(video => {
        video.viewers = Math.floor(Math.random() * 40);
        this.peopleWatching += video.viewers
      })
      await new Promise(resolve => setTimeout(resolve, 10000));
    }
  }

  /* canvas matrix modal*/
  async openCanvas() {
    const canvasMatrixModal: Components.IonModal = await this.modalCtrl.create({
      component: CanvasMatrixModalPage,
      showBackdrop: false,
      componentProps: {}
    });
    await canvasMatrixModal.present();
  }

  /* chat modal*/
  async openChat() {
    const chat: Components.IonModal = await this.modalCtrl.create({
      component: ChatModalPage,
      showBackdrop: false,
      componentProps: {}
    });
    await chat.present();
  }

}

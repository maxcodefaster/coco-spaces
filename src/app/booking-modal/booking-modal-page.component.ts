import {AfterViewInit, Component, ElementRef, ViewChild} from '@angular/core';
import {ModalController, ToastController} from '@ionic/angular';
import {formatDate} from "@angular/common";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-booking-modal',
  templateUrl: './booking-modal-page.component.html',
  styleUrls: ['./booking-modal-page.component.scss'],
})
export class BookingModalPage implements AfterViewInit {
  now: string = formatDate(new Date(), 'yyyy-MM-dd', 'en-US');
  duration: string = '01:30';
  submitted = false;
  form: FormGroup;

  constructor(
    private modalCtrl: ModalController,
    private toastCtrl: ToastController,
    private fb: FormBuilder,
  ) {
    this.form = this.fb.group({
      title: new FormControl('', [
        Validators.required,
      ]),
      startDate: new FormControl('', [
        Validators.required,
      ]),
      duration: new FormControl('', [
        Validators.required,
      ]),
    });
  }

  async ngAfterViewInit() {
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.form.controls;
  }

  async dismissModal() {
    this.modalCtrl.dismiss();
  }

  saveForm() {
    this.submitted = true;
    if (this.form.valid) {
      this.dismissModal()
    } else {
      this.formErrorToast()
    }
  }

  hasFormError(formField) {
    return !this.f[formField].valid && this.submitted;
  }

  async formErrorToast() {
    const toast = await this.toastCtrl.create({
      header: 'Please fill in required form fields',
      duration: 2000,
      position: 'bottom',
      translucent: true,
      buttons: [
        {
          side: 'start',
          icon: 'warning-outline'
        }
      ]
    });
    toast.present();
  }

  resetForm(form: FormGroup) {
    form.reset();
    this.submitted = false;
    Object.keys(form.controls).forEach(key => {
      form.get(key).setErrors(null);
    });
  }

  updateForm(e) {
    console.log(e)
  }


}

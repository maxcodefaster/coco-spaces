import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'location',
        loadChildren: () => import('../location/location.module').then(m => m.Tab2PageModule)
      },
      {
        path: 'immerse',
        loadChildren: () => import('../immerse/immerse.module').then(m => m.Tab3PageModule)
      },
      {
        path: '',
        redirectTo: '/tabs/immerse',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/immerse',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}

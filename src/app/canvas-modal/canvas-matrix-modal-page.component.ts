import {AfterViewInit, Component, ElementRef, ViewChild} from '@angular/core';
import {ModalController} from '@ionic/angular';
import * as Pubnub from 'pubnub';

@Component({
  selector: 'app-chat-modal',
  templateUrl: './canvas-matrix-modal-page.component.html',
  styleUrls: ['./canvas-matrix-modal-page.component.scss'],
})
export class CanvasMatrixModalPage implements AfterViewInit {
  @ViewChild('drawCanvas', {read: ElementRef, static: false}) drawCanvas: ElementRef<HTMLCanvasElement>;
  // pubnub
  channel: string = 'draw';
  pubnub: Pubnub;
  uuid: string;

  // local vars
  ctx: CanvasRenderingContext2D;
  isActive: boolean = false;
  color: string = '#EC5C0D'
  plots: any[] = [];
  occupancy: number = 0;
  isTouchSupported: boolean
  isPointerSupported: boolean
  isMSPointerSupported: boolean
  boundStart: any;
  boundMove: any;
  boundTouchEnd: any;

  constructor(
    private modalCtrl: ModalController,
  ) {
    this.isTouchSupported = 'ontouchstart' in window;
    this.isPointerSupported = navigator.pointerEnabled;
    this.isMSPointerSupported = navigator.msPointerEnabled;
    this.boundStart = this.startDraw.bind(this);
    this.boundMove = this.draw.bind(this);
    this.boundTouchEnd = this.endDraw.bind(this);
  }

  async ngAfterViewInit() {
    this.initDrawCanvas()
  }

  async dismissModal() {
    this.modalCtrl.dismiss();
  }

  initDrawCanvas() {
    this.ctx = this.drawCanvas.nativeElement.getContext('2d');

    this.drawCanvas.nativeElement.width = Math.min(document.documentElement.clientWidth, window.innerWidth || 300);
    this.drawCanvas.nativeElement.height = Math.min(document.documentElement.clientHeight, window.innerHeight || 300) - 60;

    this.ctx.strokeStyle = this.color;
    this.ctx.lineWidth = Number('3');
    this.ctx.lineCap = this.ctx.lineJoin = 'round';

    /* Mouse and touch events */
    let downEvent = this.isTouchSupported ? 'touchstart' : (this.isPointerSupported ? 'pointerdown' : (this.isMSPointerSupported ? 'MSPointerDown' : 'mousedown'));
    let moveEvent = this.isTouchSupported ? 'touchmove' : (this.isPointerSupported ? 'pointermove' : (this.isMSPointerSupported ? 'MSPointerMove' : 'mousemove'));
    let upEvent = this.isTouchSupported ? 'touchend' : (this.isPointerSupported ? 'pointerup' : (this.isMSPointerSupported ? 'MSPointerUp' : 'mouseup'));

    this.drawCanvas.nativeElement.addEventListener(downEvent, this.boundStart, false);
    this.drawCanvas.nativeElement.addEventListener(moveEvent, this.boundMove, false);
    this.drawCanvas.nativeElement.addEventListener(upEvent, this.boundTouchEnd, false);

    /* PubNub */
    this.uuid = Pubnub.generateUUID();
    this.pubnub = new Pubnub({
      publishKey: 'pub-c-57d392bc-32e7-4ce3-b5c5-f219935decf1',
      subscribeKey: 'sub-c-976a84fc-2d67-11eb-a9aa-e23bcc63a965',
      ssl: true,
      uuid: this.uuid
    });

    this.pubnub.subscribe({
      channels: [this.channel],
      withPresence: true
    });

    this.pubnub.addListener({
      message: (event) => {
        this.drawFromStream(event.message.content)
      },
      presence: (event) => {
        this.occupancy = event.occupancy
      }
    })

    this.pubnub.history(
      {
        channel: this.channel,
        count: 50,
        stringifiedTimeToken: true,
      },
      (status, response) => {
        if (response) {
          for (let msg of response.messages) {
            this.drawFromStream(msg.entry.content)
          }
        }
      }
    );
  }

  publish(data) {
    this.pubnub.publish({
      channel: this.channel,
      message: {"sender": this.uuid, "content": data}
    });
  }

  /* Draw on canvas */
  drawOnCanvas(color, plots) {
    this.ctx.strokeStyle = color;
    this.ctx.beginPath();
    this.ctx.moveTo(plots[0].x, plots[0].y);

    for (let i = 1; i < plots.length; i++) {
      this.ctx.lineTo(plots[i].x, plots[i].y);
    }
    this.ctx.stroke();
  }

  drawFromStream(message) {
    if (!message || message.plots.length < 1) return;
    this.drawOnCanvas(message.color, message.plots);
  }

  draw(e) {
    e.preventDefault(); // prevent continuous touch event process e.g. scrolling!
    if (!this.isActive) return;

    let x: number = this.isTouchSupported ? (e.targetTouches[0].pageX - this.drawCanvas.nativeElement.offsetLeft) : (e.offsetX || e.layerX - this.drawCanvas.nativeElement.offsetLeft);
    let y: number = this.isTouchSupported ? (e.targetTouches[0].pageY - this.drawCanvas.nativeElement.offsetTop) : (e.offsetY || e.layerY - this.drawCanvas.nativeElement.offsetTop);

    this.plots.push({x: (x << 0), y: (y << 0)}); // round numbers for touch screens

    this.drawOnCanvas(this.color, this.plots);
  }

  startDraw(e) {
    e.preventDefault();
    this.isActive = true;
  }

  endDraw(e) {
    e.preventDefault();
    this.isActive = false;

    this.publish({
      color: this.color,
      plots: this.plots
    });

    this.plots = [];
  }

}

import {Component, Input, OnInit} from '@angular/core';
import {Platform} from "@ionic/angular";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  @Input() activeSite: string = '';
  isMobile: boolean = false

  constructor(
    private platform: Platform
  ) {
  }

  ngOnInit() {
    if (this.platform.is('mobile') || this.platform.is('android')) {
      this.isMobile = true;
    }
  }

}

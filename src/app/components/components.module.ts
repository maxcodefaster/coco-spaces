import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {IonicModule} from '@ionic/angular';

import {FormsModule} from '@angular/forms';
import {WebglRendererComponent} from "./webgl-renderer/webgl-renderer.component";
import {RoomListComponent} from "./room-list/room-list.component";
import {HeaderComponent} from "./header/header.component";

@NgModule({
  declarations: [
    HeaderComponent,
    WebglRendererComponent,
    RoomListComponent
  ],
  imports: [CommonModule, IonicModule, FormsModule],
  exports: [
    HeaderComponent,
    WebglRendererComponent,
    RoomListComponent,
  ]
})
export class ComponentsModule {
}

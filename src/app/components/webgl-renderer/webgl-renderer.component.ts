import {AfterViewInit, Component, ElementRef, Input, ViewChild} from '@angular/core';
import {EngineService} from "./engine.service";

@Component({
  selector: 'app-webgl-renderer',
  templateUrl: './webgl-renderer.component.html',
  styleUrls: ['./webgl-renderer.component.scss'],
})
export class WebglRendererComponent implements AfterViewInit {
  @Input() glbFile: string;
  @ViewChild('rendererCanvas', {read: ElementRef, static: false}) rendererCanvas: ElementRef<HTMLCanvasElement>;

  constructor(private engServ: EngineService) {
  }

  public ngAfterViewInit(): void {
    this.engServ.createScene(this.rendererCanvas, this.glbFile);
    this.engServ.animate();
  }

}

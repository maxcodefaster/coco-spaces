import {Component, OnInit} from '@angular/core';
import {ModalController} from "@ionic/angular";
import {Components} from "@ionic/core";
import * as Pubnub from "pubnub";
import {UserSettingsService} from "../../utils/user-settings.service";
import {Room, RoomList22, RoomList28} from "./room-list";
import {BookingModalPage} from "../../booking-modal/booking-modal-page.component";

@Component({
  selector: 'app-room-list',
  templateUrl: './room-list.component.html',
  styleUrls: ['./room-list.component.scss'],
})
export class RoomListComponent implements OnInit {
  // local vars
  roomLists: Room[][] = [];
  userName: string

  // pubnub
  channel: string = 'roomList';
  pubnub: Pubnub;
  uuid: string;

  // pic zoom
  sliderOpts = {
    zoom: {
      maxRatio: 2
    }
  }

  constructor(
    private modalCtrl: ModalController,
    private userSettings: UserSettingsService
  ) {
    this.roomLists.push(RoomList22, RoomList28)
  }

  async ngOnInit() {
    this.userName = await this.userSettings.getUserName()
    this.uuid = await this.userSettings.getUuid()
    for (const roomList of this.roomLists) {
      this.checkAvailability(roomList)
    }
  }

  checkAvailability(roomList: Room[]) {
    const timeNow = new Date();
    console.log(timeNow)
    // check all rooms for bookings
    for (const room of roomList) {
      // check all bookings
      for (const [i, booking] of room.bookings.entries())
        // check if room is free
        if (booking.bookingStart <= timeNow && timeNow <= booking.bookingEnd) {
          console.log('room booked')
        } else if (booking.bookingEnd <= timeNow) {
          // delete booking if event is already over
          delete room.bookings[i]
        }
    }
  }

  /* booking modal*/
  async newBooking(room: Room) {
    const bookingModal: Components.IonModal = await this.modalCtrl.create({
      component: BookingModalPage,
      componentProps: {}
    });
    await bookingModal.present();
  }

  changeIcon(room: Room) {

  }

}

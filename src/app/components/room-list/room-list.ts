type cocoLocation = 22 | 28

export class Room {
  public roomNum: number;
  public location: cocoLocation;
  public bookings: Booking[]

  constructor(roomNum: number, location: cocoLocation, bookings: Booking[]) {
    this.roomNum = roomNum;
    this.location = location;
    this.bookings = bookings
  }
}

export class Mood {
  public title: string;
  public icon: string;

  constructor(title: string, icon: string) {
    this.title = title;
    this.icon = icon;
  }
}

export class Booking {
  public creator: string;
  public mood: Mood;
  public bookingStart: Date;
  public bookingEnd: Date;

  constructor(creator: string, mood: Mood, bookingStart: Date, bookingEnd: Date) {
    this.creator = creator;
    this.mood = mood;
    this.bookingStart = bookingStart;
    this.bookingEnd = bookingEnd;
  }
}

export const RoomList22: Room[] = [
  // Schanzenstraße 22
  new Room(1, 22, []),
  new Room(2, 22, []),
  new Room(3, 22, []),
  new Room(4, 22, []),
  new Room(5, 22, []),
  new Room(6, 22, []),
  new Room(7, 22, []),
  new Room(8, 22, []),
  new Room(9, 22, []),
]

export const RoomList28: Room[] = [
  // Schanzenstraße 28
  new Room(1, 28, []),
  new Room(2, 28, []),
  new Room(3, 28, []),
  new Room(4, 28, []),
  new Room(5, 28, []),
  new Room(6, 28, []),
  new Room(7, 28, []),
  new Room(8, 28, []),
  new Room(9, 28, []),
  new Room(10, 28, [])
]
